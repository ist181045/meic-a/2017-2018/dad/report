\documentclass[times,10pt,twocolumn]{article} 
\usepackage{latex8}
\usepackage[utf8]{inputenc}
\usepackage{authblk}
\usepackage{times}

\pagestyle{empty}

%------------------------------------------------------------------------- 
\begin{document}

\title{DAD’s Online Gaming Platform (DAD-OGP)}
\author{João Oliveira, Pedro Cerejo, Rui Ventura\\
  Instituto Superior Técnico\\
  Information Systems and Computer Engineering - Alameda\\
  Design and Implementation of Distributed Applications\\
  joao.alexandre.oliveira@tecnico.ulisboa.pt,
  pedro.cerejo@tecnico.ulisboa.pt,
  rui.ventura@tecnico.ulisboa.pt
}

\maketitle
\thispagestyle{empty}

\begin{abstract}
The DAD project aims at implementing a simplified (and therefore far from
complete) fault-tolerant real-time distributed gaming platform.
\end{abstract}



%------------------------------------------------------------------------- 
\Section{Introduction}

The goal of this project is to implement a distributed gaming platform for an
adaptation of the game Pacman. The game is to be implemented in a classic
Client-Server architecture, with the server being responsible for computing all
the actions from the players and synchronizing their instances of the game.
Besides the game, a chat is also to be implemented, although in a different way,
since it would be a component that is (mostly) independent of the server,
handled in a Peer-to-peer fashion, by the clients themselves.

Being a distributed system, fault-tolerance is a big concern, and to this end
the replication of the server and other fault-tolerant measures are to be
implemented.
  

%------------------------------------------------------------------------- 
\Section{Solution}

The solution to the presented problem needs to be both fault-tolerant to a
reasonable extent and somewhat scalable in terms of involved parties (e.g.
number of players and number of server replicas). That being the case, the
solution is composed of three components:

\begin{enumerate}
  \item The chat, through which the clients communicate with each other
  \item The communication between the primary server and its replicas (fault
    tolerance)
  \item The game itself, through which the clients and the server communicate
    and vice-versa
\end{enumerate}

In all of these components, most of the remote calls (Server to Client, Client
to Client and Server to Server) are asynchronous, in a way as to not stop the
flow of the application while waiting for the return of a function.


%------------------------------------------------------------------------- 
\SubSection{Chat}

The chat is the way all players can communicate between themselves during the
gaming session. In our solution there is no contact with the server whatsoever
in order to achieve this communication (except for the initial contact with the
server to get all of the other clients’ information). After this exchange of
information from the server with the clients, all the communication is then
handled between the clients themselves in a Peer-to-peer style.

When a player wishes to send a message to all the others, the client simply
broadcasts this message to all of the other clients that are in the gaming
session. We chose to implement the client to client communication this way
merely due to its simplicity. This way of communicating would obviously never be
very scalable, but due to the nature of the communication being a chat in a game
in which not a huge amount of players would take part in, a solution closer to a
structured P2P would increase the complexity (especially considering the fault
tolerance measures that would have to be taken).

Regarding the causal order guarantees, the solution we opted for was using
vector clocks. These vectors (one for each client) have one entry per client
involved in the communication and are all initialized at zero. Each time a
client broadcasts a message he increments his entry in the vector and broadcasts
it with the message. Upon receiving a message a client can use the vector that
comes with it to ensure that it is the most recent message sent from the sender,
and that at the time he sent it, no other messages from other clients had been
processed in the sender’s side and not in the receiver’s side. This addition
allows causal order to be maintained, and that no messages are seen out of
context.


%------------------------------------------------------------------------- 
\SubSection{Game}

The second part of the solution consists in the actual distributed game. Once
all clients have connected to the server, it sends them the information they
need for the gaming session (e.g. other clients’ information, starting player
positions, etc…). After all the clients received this information the server
signals them that the game has started and starts the game.

Once they have received the signal that the game has started, the clients start
their game cycle. From the client side the game is actually quite simple. Each
round, each client sends the direction of its movement (up, down, left or right)
to the server (or nothing, in case he’s stopped). Every time these remote calls
are made from the clients’ side, the server updates that client’s next
direction.

All of the game logic, though, is on the side of the server. Each round, it
computes all the last movements of the clients, updating the players’ scores and
checking if any of them died. It then proceeds to sending the game’s updates to
all of the clients, generate and save a state containing all the game’s
information after that round. This occurs until the game is over (e.g. all
players die, all coins are picked up). When receiving a state from the server,
the clients update their own game state (e.g. player positions, score, etc…).




%------------------------------------------------------------------------ 
\SubSection{Fault Tolerance}

Finally, the solution also contains some fault-tolerance features, the main one
being the replication of the server. Any server that is launched after the first
one, becomes a secondary server (a replica), and periodically (every 3 seconds)
send a signal (ImAlive) to every other replica. At the same time, every 3
seconds (plus some more time to account for network travel time) each replica
checks to see if received an ImAlive from every one of the other replicas, to
ensure that they are still running properly. Whichever server is the primary at
the moment is the only one that does not receive any ImAlive signals, and merely
sends them to the other replicas.

If any replica notices that since the last check, it has not received an ImAlive
from one (or more) of the other replicas, they assume that replica to be dead,
and continue assuming so until receiving another ImAlive from that very server.
If the server that died happens to be the primary server, all the remaining
servers enter a leader selection procedure, and attempt to elect a new primary
server. 

To this end, we have opted to employ an adaptation of a ring-based algorithm.
Like in a traditional ring-based algorithm, the replicas are displayed in a ring
and send their bid for leader to the next node in the ring, with the exception
here being that given that all replicas have all of the other replicas’ ids,
they send the biggest id from the servers that they don’t presume to be dead.
Hence, if a replica receives a message with a different id from the one that it
currently has as a choice for the next leader, the one it received becomes its
new choice, and it propagates it to the next node in the ring. Upon receiving
the same id as the one it has, the replica know that all the nodes have agreed
to the server with that id as the new leader. The reason behind our adaptation
of this algorithm was to not only have the different servers send less messages
(by immediately selecting the highest id they have), but also to avoid having a
time-based algorithm.

As for the actual communication between the servers, they implement a three
strike policy, being that they attempt to send each message three times, and
after which assume the receiving server to be dead. The clients also implement
this policy when communicating with each other, meaning that after three failed
attempts to send a message to another client, it is assumed to be dead. On the
other hand, when communicating with the clients, the server assumes they are
dead after the first failure in communication.


%-------------------------------------------------------------------------
\Section{Evaluation and conclusion}

Overall, the implementation of the solution is satisfactory. It can handle
several clients and servers connected with no real impact on performance.
Furthermore, it is (mostly) fault tolerant. The gaming session is maintained
until all clients are either disconnected or dead, in the Peer-to-peer like
chat, causal order is guaranteed and the gaming sessions last until there are no
more servers available.

On the other hand, the implementation also has some flaws. When the primary
server crashes, it could take up to 6 seconds for the replicas to notice that
fault, and act accordingly, thus hurting the players’ experience. Due to
implementation specifics and limitations, players can’t initially connect to the
server and start playing if the original primary server has died, and there may
be other flaws that we simply aren’t aware of due to lack of extensive testing.


%------------------------------------------------------------------------- 
%\nocite{*}
%\bibliographystyle{latex8}
%\bibliography{latex8}

\end{document}
  